<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use League\Flysystem\Exception;
use App\Helpers\Matriz;

class TestController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index() {
        return view("test/index");
    }

    public function selection(Request $request) {
        $objMatriz = new Matriz();
        $quantity = $objMatriz->processData($request->selection);
        if (is_integer($quantity)) {
            $return["quantity"] = $quantity;
            $return["typeMatriz"] = $request->selection;
            $return["messageQuantity"] = '<div class="alert alert-info">Posee <b>' . $return["quantity"] . "</b> aparicion" . (($return["quantity"] > 1) ? "es" : "") . " de la palabra clave <b>OIE</b>.</div>";
            $return["messageMatriz"] = '<div class="alert alert-info"> La matriz <b>' . $request->selection . "</b>. Posee <b>" . $return["quantity"] . "</b> aparicion" . (($return["quantity"] > 1) ? "es" : "") . " de la palabra clave <b>OIE</b>.</div>";
        } else {
            $return["messageMatriz"] = $quantity;
        }
        die(json_encode($return));
    }

}
