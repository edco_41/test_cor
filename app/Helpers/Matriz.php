<?php

namespace App\Helpers;

/**
 * Esta clase podra realizar todos los procesos de lamatriz
 *
 * @author Eder Colorado 
 */
class Matriz {

    /**
     * Proceso de la matriz que se seleccion en la vista
     * 
     * @dataProvider matrizData Funcion que posee los datos de la matriz de la vista
     * @param String $typeMatriz Contiene el tipo de matriz a evaluar.
     * @return int Retorna la cantidad de veces que se encuentra OIE en la matriz seleccionada
     */
    public function processData($typeMatriz) {
        try {
            $matriz = $this->data($typeMatriz);
            if (is_array($matriz)) {
                return $this->read($matriz);
            } else {
                throw new Exception('<div class="alert alert-danger">Seleccione una matriz.</div>');
            }
        } catch (Exception $exc) {
            return $exc->getMessage();
        }
    }

    private function data($matrizKey) {
        $matriz = array();
        $matrizData = array("3x3" => array(
                array("O", "I", "E"),
                array("I", "I", "X"),
                array("E", "X", "E")
            )
            , "1x10" => array(
                array("E", "I", "O", "I", "E", "I", "O", "E", "I", "O")
            )
            , "5x5" => array(
                array("E", "A", "E", "A", "E"),
                array("A", "I", "I", "I", "A"),
                array("E", "I", "O", "I", "E"),
                array("A", "I", "I", "I", "A"),
                array("E", "A", "E", "A", "E"),
            )
            , "7x2" => array(
                array("O", "X"),
                array("I", "O"),
                array("E", "X"),
                array("I", "I"),
                array("O", "X"),
                array("I", "E"),
                array("E", "X")
            )
        );
        // Se evalua la seleccion de la matriz
        foreach ($matrizData as $key => $value) {
            if (strtolower(trim($matrizKey)) == strtolower(trim($key))) {
                $matriz = $value;
                break;
            } else {
                $matriz = NULL;
            }
        }
        return $matriz;
    }

    private function rowLeftToRigth($matriz) {
        $quantity = 0;
        $writeArray = array();
        foreach ($matriz as $row) {
            foreach ($row as $value) {

                if (empty($writeArray[0]) && $value == "O") {
                    $writeArray[0] = $value;
                } else {

                    if (!empty($writeArray[0]) && empty($writeArray[1]) && $value == "I") {
                        $writeArray[1] = $value;
                    } else {

                        if (!empty($writeArray[0]) && !empty($writeArray[1]) && empty($writeArray[2]) && $value == "E") {
                            $writeArray[2] = $value;
                        } else {
                            $writeArray = array();
                        }
                    }
                }

                if (count($writeArray) == 3) {
                    $quantity++;
                    $writeArray = array();
                }
            }
        }
        return $quantity;
    }

    private function rowRigthToLeft($matriz) {
        //        Cantidad de apariciones de palabra OIE
        $quantity = 0;
        $writeArray = array();
        foreach ($matriz as $row) {
            $row = array_reverse($row);
            foreach ($row as $value) {

                if (empty($writeArray[0]) && $value == "O") {
                    $writeArray[0] = $value;
                } else {

                    if (!empty($writeArray[0]) && empty($writeArray[1]) && $value == "I") {
                        $writeArray[1] = $value;
                    } else {

                        if (!empty($writeArray[0]) && !empty($writeArray[1]) && empty($writeArray[2]) && $value == "E") {
                            $writeArray[2] = $value;
                        } else {
                            $writeArray = array();
                        }
                    }
                }

                if (count($writeArray) == 3) {
                    $quantity++;
                    $writeArray = array();
                }
            }
        }
        return $quantity;
    }

    private function columnUpToDown($matriz) {
        //        Cantidad de apariciones de palabra OIE
        $quantity = 0;
        $writeArray = array();

        $elements = count($matriz, COUNT_RECURSIVE);
        $rows = count($matriz);
        $elementsRow = ($elements - $rows) / $rows;

        for ($e = 0; $e < $elementsRow; $e++) {
            for ($f = 0; $f < $rows; $f++) {

                // Evaluacion de cantidad de apariciones
                $writeArray = $this->quantity($writeArray, $matriz, $f, $e);

                if (count($writeArray) == 3) {
                    $quantity++;
                    $writeArray = array();
                }
            }
            $writeArray = array();
        }
        return $quantity;
    }

    private function columnDownToUp($matriz) {
        //        Cantidad de apariciones de palabra OIE
        $quantity = 0;
        $writeArray = array();

        $elements = count($matriz, COUNT_RECURSIVE);
        $rows = count($matriz);
        $elementsRow = ($elements - $rows) / $rows;

        for ($e = $elementsRow - 1; $e >= 0; $e--) {
            for ($f = $rows - 1; $f >= 0; $f--) {

                // Evaluacion de cantidad de apariciones
                $writeArray = $this->quantity($writeArray, $matriz, $f, $e);

                if (count($writeArray) == 3) {
                    $quantity++;
                    $writeArray = array();
                }
            }
            $writeArray = array();
        }
        return $quantity;
    }

    private function diagonalLeftToRigthUpToDown($matriz) {
        //        Cantidad de apariciones de palabra OIE
        $quantity = 0;
        $writeArray = array();

        $row = 0;
        $column = 0;
        $rowTotal = count($matriz);

        do {
            $columnTotal = count($matriz[$row]);

            while ($column < $columnTotal) {
                if ($row == $column) {
                    // Evaluacion de cantidad de apariciones
                    $writeArray = $this->quantity($writeArray, $matriz, $row, $column);
                    if (count($writeArray) == 3) {
                        $quantity++;
                    }
                    $column++;
                    break;
                }
            }
            $row++;
        } while ($row < $rowTotal);

        return $quantity;
    }

    private function diagonalLeftToRigthDownToUp($matriz) {
        //        Cantidad de apariciones de palabra OIE
        $quantity = 0;
        $writeArray = array();

        $row = count($matriz);
        $rowTotal = 0;

        do {
            $row--;
            $column = count($matriz[$row]);
            $columnTotal = 0;

            while ($column > $columnTotal) {
                $column--;

                if ($row == $column) {
                    // Evaluacion de cantidad de apariciones
                    $writeArray = $this->quantity($writeArray, $matriz, $row, $column);
                    if (count($writeArray) == 3) {
                        $quantity++;
                    }
                }
            }
        } while ($row > $rowTotal);

        return $quantity;
    }

    private function diagonalRigthToLeftUpToDown($matriz) {
        //        Cantidad de apariciones de palabra OIE
        $quantity = 0;
        $writeArray = array();

        $rowTotal = count($matriz);
        $columnFlag = TRUE;
        for ($row = 0; $row < $rowTotal; $row++) {

            if ($columnFlag) {
                $column = (count($matriz[$row]) - 1);
            } else {
                $column = ($column - 1);
            }

            while ($column > (-1)) {

                if (isset($matriz[$row][$column])) {
                    // Evaluacion de cantidad de apariciones
                    $writeArray = $this->quantity($writeArray, $matriz, $row, $column);
                    if (count($writeArray) == 3) {
                        $quantity++;
                    }
//                    echo "<br/>" . $matriz[$row][$column] . "<br/> ROW -> " . ($row + 1) . " - COLUMN -> " . ($column + 1);
                    $columnFlag = FALSE;
                    break;
                }
                $column--;
            }
        }
        return $quantity;
    }

    private function diagonalRigthToLeftDownToUp($matriz) {
        //        Cantidad de apariciones de palabra OIE
        $quantity = 0;
        $writeArray = array();

        $rowTotal = (count($matriz) - 1);
        $columnFlag = TRUE;
        for ($row = $rowTotal; $row > (-1); $row--) {

            $columnTotal = (count($matriz[$row]) - 1);

            if ($columnFlag) {
                $column = 0;
            } else {
                $column = $column + 1;
            }

            while ($column <= $columnTotal) {
                if (isset($matriz[$row][$column])) {
                    // Evaluacion de cantidad de apariciones
                    $writeArray = $this->quantity($writeArray, $matriz, $row, $column);
                    if (count($writeArray) == 3) {
                        $quantity++;
                    }
//                    echo "<br/>" . $matriz[$row][$column] . "<br/> ROW -> " . ($row + 1) . " - COLUMN -> " . ($column + 1);
                    $columnFlag = FALSE;
                    break;
                }
                $column++;
            }
        }
        return $quantity;
    }

    private function quantity($writeArray, $matriz, $row, $column) {
        //   Evaluacion y conteo
        if (empty($writeArray[0]) && $matriz[$row][$column] == "O") {
            $writeArray[0] = $matriz[$row][$column];
        } else {

            if (!empty($writeArray[0]) && empty($writeArray[1]) && $matriz[$row][$column] == "I") {
                $writeArray[1] = $matriz[$row][$column];
            } else {

                if (!empty($writeArray[0]) && !empty($writeArray[1]) && empty($writeArray[2]) && $matriz[$row][$column] == "E") {
                    $writeArray[2] = $matriz[$row][$column];
                } else {
                    $writeArray = array();
                }
            }
        }
        return $writeArray;
    }

    private function read($matriz) {
        // Llamado de metodos para las lecturas de las matrices
        $rowLeftToRigth = $this->rowLeftToRigth($matriz);
        $rowRigthToLeft = $this->rowRigthToLeft($matriz);
        $columnUpToDown = $this->columnUpToDown($matriz);
        $columnDownToUp = $this->columnDownToUp($matriz);

        $diagonalLeftToRigthUpToDown = $this->diagonalLeftToRigthUpToDown($matriz);
        $diagonalLeftToRigthDownToUp = $this->diagonalLeftToRigthDownToUp($matriz);
        $diagonalRigthToLeftUpToDown = $this->diagonalRigthToLeftUpToDown($matriz);
        $diagonalRigthToLeftDownToUp = $this->diagonalRigthToLeftDownToUp($matriz);

        $quantityRow = $rowLeftToRigth + $rowRigthToLeft;
        $quantityColumn = $columnUpToDown + $columnDownToUp;
        $quantityDiagonal = $diagonalLeftToRigthUpToDown + $diagonalLeftToRigthDownToUp + $diagonalRigthToLeftUpToDown + $diagonalRigthToLeftDownToUp;

        $quantity = $quantityRow + $quantityColumn + $quantityDiagonal;
        return $quantity;
    }

}
