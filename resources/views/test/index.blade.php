@extends('app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div id="message"></div>
            <form id="form" action="{{ route('selection') }}" method="POST">
            <label>Listado de matrices</label>
            <input id="token" type="hidden" name="_token" value="{{ csrf_token() }}">
            <select id="selectionMatriz">
                <option value="">Seleccione</option>
                <option value="3x3" >3x3</option>
                <option value="1x10" >1x10</option>
                <option value="5x5">5x5</option>
                <option value="7x2">7x2</option>
            </select>
            <button type="button" id="sendMatriz">Enviar</button>
            </form>
            <hr/>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <label class="col-md-2 h2">3x3</label><div class="col-md-10 quantity" id="3x3"></div>
            <div class="col-md-12">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th>&nbsp;</th>
                            <th>1</th>
                            <th>2</th>
                            <th>3</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <th>1</th>
                            <td>O</td>
                            <td>I</td>
                            <td>E</td>
                        </tr>
                        <tr>
                            <th>2</th>
                            <td>I</td>
                            <td>I</td>
                            <td>X</td>
                        </tr>
                        <tr>
                            <th>3</th>
                            <td>E</td>
                            <td>X</td>
                            <td>E</td>
                        </tr>
                    </tbody>
                    <tfoot>
                        <tr>
                            <th>&nbsp;</th>
                            <th>1</th>
                            <th>2</th>
                            <th>3</th>
                        </tr>
                    </tfoot>
                </table>

            </div>
        </div>
        <div class="col-md-6">
            <label class="col-md-2 h2">1x10</label><div class="col-md-10 quantity" id="1x10"></div>
            <div class="col-md-12">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th>&nbsp;</th>
                            <th>1</th>
                            <th>2</th>
                            <th>3</th>
                            <th>4</th>
                            <th>5</th>
                            <th>6</th>
                            <th>7</th>
                            <th>8</th>
                            <th>9</th>
                            <th>10</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <th>1</th>
                            <td>E</td>
                            <td>I</td>
                            <td>O</td>
                            <td>I</td>
                            <td>E</td>
                            <td>I</td>
                            <td>O</td>
                            <td>E</td>
                            <td>I</td>
                            <td>O</td>
                        </tr>
                    </tbody>
                    <tfoot>
                        <tr>
                            <th>&nbsp;</th>
                            <th>1</th>
                            <th>2</th>
                            <th>3</th>
                            <th>4</th>
                            <th>5</th>
                            <th>6</th>
                            <th>7</th>
                            <th>8</th>
                            <th>9</th>
                            <th>10</th>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <label class="col-md-2 h2">5x5</label><div class="col-md-10 quantity" id="5x5"></div>
            <div class="col-md-12">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th>&nbsp;</th>
                            <th>1</th>
                            <th>2</th>
                            <th>3</th>
                            <th>4</th>
                            <th>5</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <th>1</th>
                            <td>E</td>
                            <td>A</td>
                            <td>E</td>
                            <td>A</td>
                            <td>E</td>
                        </tr>
                        <tr>
                            <th>2</th>
                            <td>A</td>
                            <td>I</td>
                            <td>I</td>
                            <td>I</td>
                            <td>A</td>
                        </tr>
                        <tr>
                            <th>3</th>
                            <td>E</td>
                            <td>I</td>
                            <td>O</td>
                            <td>I</td>
                            <td>E</td>
                        </tr>
                        <tr>
                            <th>4</th>
                            <td>A</td>
                            <td>I</td>
                            <td>I</td>
                            <td>I</td>
                            <td>A</td>
                        </tr>
                        <tr>
                            <th>5</th>
                            <td>E</td>
                            <td>A</td>
                            <td>E</td>
                            <td>A</td>
                            <td>E</td>
                        </tr>
                    </tbody>
                    <tfoot>
                        <tr>
                            <th>&nbsp;</th>
                            <th>1</th>
                            <th>2</th>
                            <th>3</th>
                            <th>4</th>
                            <th>5</th>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
        <div class="col-md-6">
            <label class="col-md-2 h3">7x2</label><div class="col-md-10 quantity" id="7x2"></div>
            <div class="col-md-12">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th>&nbsp;</th>
                            <th>1</th>
                            <th>2</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <th>1</th>
                            <td>O</td>
                            <td>X</td>
                        </tr>
                        <tr>
                            <th>2</th>
                            <td>I</td>
                            <td>O</td>
                        </tr>
                        <tr>
                            <th>3</th>
                            <td>E</td>
                            <td>X</td>
                        </tr>
                        <tr>
                            <th>4</th>
                            <td>I</td>
                            <td>I</td>
                        </tr>
                        <tr>
                            <th>5</th>
                            <td>O</td>
                            <td>X</td>
                        </tr>
                        <tr>
                            <th>6</th>
                            <td>I</td>
                            <td>E</td>
                        </tr>
                        <tr>
                            <th>7</th>
                            <td>E</td>
                            <td>X</td>
                        </tr>
                    </tbody>
                    <tfoot>
                        <tr>
                            <th>&nbsp;</th>
                            <th>1</th>
                            <th>2</th>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection
