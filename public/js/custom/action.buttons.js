$(document).ready(function () {

    $("#sendMatriz").click(function () {
        selection = $("#selectionMatriz").val()
        if (selection != '' && selection != undefined) {
            sendMatriz($("#token").val(), selection);
        } else {
            $(".quantity").html('');
            $("#message").html('<div class="alert alert-danger">Seleccione una matriz.</div>');
        }
    });

});

function sendMatriz(token, selection) {
    $.ajax({
        method: $("#form").attr('method'),
        url: $("#form").attr('action'),
        dataType: "JSON",
        data: {_token: token, selection: selection},
        success: function (data, textStatus, jqXHR) {
            $(".quantity").html('');
            $("#message").html('');
            $("#message").html(data['messageMatriz']);
            $("#" + data['typeMatriz']).html(data['messageQuantity']);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            $(".quantity").html('');
            $("#message").html('');
            $("#message").html(data['messageMatriz']);
        }
    });
}
